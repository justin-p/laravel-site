Laravel-site
================
[![pipeline status](https://gitlab.com/justin-p/laravel-site/badges/master/pipeline.svg)](https://gitlab.com/justin-p/laravel-site/commits/master)

During a setup of a development/production environment for a customer I ran into laravel. I'd heard about it, never touched it.
We ran into some problems during the migration of laravel-based webapp from the old platform to the newly build one so I decided to toy around with laravel to get a better grasp of how the shizniks works.

I also decided to use laradock since I really didn't feel like setting up nginx/mysql by hand.

## Clone this repository
git clone --recurse-submodules  https://gitlab.com/justin-p/laravel-site

## Install/setup

### Setup a laravel project and laradock

#### On the host system

```bash
choco install composer / apt install composer
composer create-project laravel/laravel laravel-site
git submodule add https://github.com/Laradock/laradock.git

cp .env.example .env
    
    DB_DATABASE=default
    DB_USERNAME=root
    DB_PASSWORD=root
    
cp laradock\env-example laradock\.env

cd laradock
docker-compose up -d nginx mysql
docker-compose exec workspace bash
```

#### inside of the workspace container

```bash
composer install
php artisan key:generate
exit
```

#### [php artisan migrate fix for mysql 8.0](https://github.com/laradock/laradock/issues/1392#issuecomment-409612243)
#### On the host system again

```bash
docker-compose exec mysql bash
```

### inside of the mysql container 

```bash
mysql -u root -p
mysql> ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'root';
mysql> ALTER USER 'root'@'%' IDENTIFIED WITH mysql_native_password BY 'root';
mysql> ALTER USER 'default'@'%' IDENTIFIED WITH mysql_native_password BY 'secret';
mysql> exit
exit
```

#### On the host system again

```bash
docker-compose exec workspace
```

#### inside of the workspace container

```bash
php artisan migrate
php artisan make:auth
```

## Update laradock

```bash
git submodule update --remote --merge
```


## [Setup CI/CD ](https://docs.gitlab.com/ee/ci/examples/laravel_with_gitlab_and_envoy/)

### Dockerfile

```dockerfile
FROM php:7.1

# Update packages
RUN apt-get update

# Install PHP and composer dependencies
RUN apt-get install -qq git curl libmcrypt-dev libjpeg-dev libpng-dev libfreetype6-dev libbz2-dev

# Clear out the local repository of retrieved package files
RUN apt-get clean

# Install needed extensions
# Here you can install any other extension that you need during the test and deployment process
RUN docker-php-ext-install mcrypt pdo_mysql zip

# Install Composer
RUN curl --silent --show-error https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Install Laravel Envoy
# RUN composer global require "laravel/envoy=~1.0"

```

### On host

```bash
docker login registry.gitlab.com
docker build -t registry.gitlab.com/justin-p/laravel-site .
docker push registry.gitlab.com/justin-p/laravel-site
```

### .gitlab-ci.yml

```yml
image: registry.gitlab.com/justin-p/laravel-site:latest

services:
  - mysql:5.7

variables:
  MYSQL_DATABASE: homestead
  MYSQL_ROOT_PASSWORD: secret
  DB_HOST: mysql
  DB_USERNAME: root

stages:
  - test

unit_test:
  stage: test
  script:
    - cp .env.example .env
    - composer install
    - php artisan key:generate
    - php artisan migrate
    - vendor/bin/phpunit
```

